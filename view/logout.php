<?php
    require_once("../config/connection.php");
    session_destroy();
    header("Location: ".BASE_URL."index.php");
    exit();