<?php
    require_once '../../modals/TipeKamar.php';
    $tipeKamar = new TipeKamar();
    $result = $tipeKamar->get_data();

    if(isset($_POST['submit']) and $_POST['submit']=='reservasi'){
        require_once '../../modals/Pemesanan.php';
        $pemesanan = new Pemesanan();

        $nm_pemesanan = $_POST['nm_pemesanan'];
        $email = $_POST['email'];
        $no_hp = $_POST['no_hp'];
        $nm_tamu = $_POST['nm_tamu'];
        $id_kamar = $_POST['id_kamar'];
        $cek_in = $_POST['cek_in'];
        $cek_out = $_POST['cek_out'];
        $jml = $_POST['jml'];

        if(empty($nm_pemesanan) AND empty($id_kamar)){
            $alert = "<script>alert('Silahkan Lengkapi Data')</script>";
            echo $alert;
        }else{
            $result = $pemesanan->insert($nm_pemesanan, $email, $no_hp, $nm_tamu, $id_kamar, $cek_in, $cek_out, $jml);
            if(isset($result)){
                header("Location:".BASE_URL."view/Template/bukti_reservasi.php");
                exit();
            }
        }

    }
?>

	
    



<!DOCTYPE html>
<html>

<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application|Pemesanan</title>

    <link href="../../public/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="../../public/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="../../public/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="../../public/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="../../public/img/favicon.png" rel="icon" type="image/png">
	<link href="../../public/img/favicon.ico" rel="shortcut icon">

	
    <link rel="stylesheet" href="../../public/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../public/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/main.css">


	<!-- sweetalert -->
	<link rel="stylesheet" href="../../public/css/lib/bootstrap-sweetalert/sweetalert.css">
	<link rel="stylesheet" href="../../public/css/separate/vendor/sweet-alert-animations.min.css">

    
    
</head>
<body class="with-center-menu">

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Form Pemesanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="../../index.php">Home</a></li>
								<li class="active">Pemesanan</li>
							</ol>
						</div>
					</div>
				</div>
		</header>
            <div class="box-typical box-typical-padding">
            <h5 class="m-t-lg with-border">Tambah Pemesanan Baru</h5>
		<form method="POST" action="">
                <div class="row">

				

					<div class="col-lg-6">
                        <fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Tipe Kamar</label>
                            <select id="cat_select" name="id_kamar" class="form-control">
								<?php while($row = $result->fetch_object()){?>
                                <option value="<?php echo $row->id_kamar?>">
                                <?php echo $row->tipe_kamar ?>
                            </option>
                                <?php
                                }
                                ?>
							</select>
						</fieldset>
					</div>
					<div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Nama Pemesan</label>
							<input type="text" class="form-control" id="nama_pemesanan" placeholder="Nama Pemesan" name="nm_pemesanan">
						</fieldset>
					</div>
				</div><!--.row-->

				<div class="row">
                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Email</label>
							<input type="text" class="form-control" id="email" placeholder="Email" name="email">
						</fieldset>
					</div>
					
                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">No Handphone</label>
							<input type="text" class="form-control" id="no_hp" placeholder="No Handphone" name="no_hp">
						</fieldset>
					</div>
				</div>


				<div class="row">
                   
                    <div class="col-lg-6">
						<div class="form-group">
							<div class='input-group date datetimepicker-1'>
                            <label class="form-label" for="exampleInputPassword1">Check In</label>
								<input type='date' class="form-control" id="check_in" name="cek_in">
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Nama Tamu</label>
							<input type="text" class="form-control" id="nama_tamu" placeholder="Nama Tamu" name="nm_tamu">
						</fieldset>
					</div>
				</div>

				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<div class='input-group date datetimepicker-1'>
                            <label class="form-label" for="exampleInputPassword1">Check Out</label>
								<input type='date' class="form-control" id="check_out" name="cek_out">
							</div>
						</div>
					</div>
                    <div class="col-lg-4">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Jumlah</label>
							<input type="text" class="form-control" id="jml_kamar" placeholder="Jumlah" name="jml">
						</fieldset>
					</div>
				</div>

                <div class="row">   
                    <div class="col-xs-12 m-t-md">
                        <input type="hidden" name="submit" value="reservasi">
                        <button type="submit" class="btn btn-rounded btn-inline btn-success-outline">Simpan</button>
                        <button type="reset" class="btn btn-rounded btn-inline btn-primary-outline">Batal</button>
                    </div>
				</div> 
				
		</form> <!-- FORM TICKET BARU -->
		
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <script src="../../public/js/lib/jquery/jquery.min.js"></script>
	<script src="../../public/js/lib/tether/tether.min.js"></script>
	<script src="../../public/js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="../../public/js/plugins.js"></script>
    <script src="../../public/js/app.js"></script>
</body>
</html>
