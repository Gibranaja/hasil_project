var table;

function init() {
    showForm(false);
    get_data();

    $("#formTambah").on("submit", function (e) {
        saveOrEdit(e);
    });

   
}


// function digunakan untuk membersihkan from pemesanan
function cleanString() {
    $("#id_fakam").val("");
    $("#tipe_kamar").val("");
    $("#fasilitas_kamar").val("");
}

// function yang digunakan untuk menapilkan form
function showForm(flag) {
    // cleanString();
    if (flag){
        $("#daftarkamar").hide();
        $("#formTambah").show();
        $("#btnTambah").hide();
    } else {
        $("#daftarkamar").show();
        $("#formTambah").hide();
        $("#btnTambah").show();
    }

}
// function yang digunakan membatalakan show form
function closeForm() {
    cleanString();
    showForm(false);
}

function get_data() {
    table = $('#tbl_list').dataTable({
        "aProcessing" : true, 
        "aServerSide" : true,
        "ajax": {
            url: "../../controller/fasilitasKamar.php?action=get_data",
            type: "POST",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        responsive: true
    }).DataTable();
}

function saveOrEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#formTambah")[0]);

    if ($('#tipe_kamar').val() == '' || $('#fasilitas_kamar').val() == ''){
        swal("Perahatian", "Silahkan Isi Data Terlebih dahulu", "warning");
    } else {
        $.ajax({
            url: "../../controller/fasilitasKamar.php?action=saveOrEdit",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {
                swal("Selamat", "Data Berhasil Disimpan", "success");
                showForm(false);
                table.ajax.reload();
            }
        });
 
    }
    // cleanString();
}

function show(id_fakam) {
    $.post("../../controller/fasilitasKamar.php?action=show", { id_fakam: id_fakam },
    function (data) {
        data = JSON.parse(data);
        showForm(true);

        $("#tipe_kamar").val(data.tipe_kamar);
        $("#fasilitas_kamar").val(data.fasilitas_kamar);
        $("#id_fakam").val(data.id_fakam);
    })
}

function delete_data(id_fakam) {

    swal({
        title: "Konfirmasi Penghapusan Data",
        text: "Data Akan Dihapus Permanen!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.post("../../controller/fasilitasKamar.php?action=delete_data", { id_fakam: id_fakam}, function (data) { })

            table.ajax.reload();

            swal("Data Telah Dihapus", {
                icon: "success",
            });
        } else {
            swal("Penghapusan Batal");
        }
    });
}



init()