<?php

    require_once "../modals/fasilitasUmum.php";

    $fasilitas_umum = new FasilitasUmum();


    // cek jika ada id_fasilitas_umum, tipekamar, keterangan pada request
    // jika ada id kamar maka cleanstring
    // jika tidk ada maka kosongkan
    $id_fasilitas_umum = isset($_POST["id_fasilitas_umum"]) ? cleanString($_POST["id_fasilitas_umum"]): "";
    $nama_fasilitas = isset($_POST["nama_fasilitas"]) ? cleanString($_POST["nama_fasilitas"]): "";
    $keterangan = isset($_POST["keterangan"]) ? cleanString($_POST["keterangan"]): "";

    // struktur kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_fasilitas_umum)){
                // jika $id-kamar tidak ada pada request, jalankan method insert
                $response = $fasilitas_umum->insert($nama_fasilitas, $keterangan);
            }else{
                // jika $id_fasilitas_umum ada, jalankan method edit
                $response = $fasilitas_umum->update($id_fasilitas_umum, $nama_fasilitas, $keterangan);
            }
            break;

            case 'get_data' :
                $response = $fasilitas_umum->get_data();

                $data = Array();

                while($row = $response->fetch_object()){
                    $data[] = array(
                        "0"=>$row->nama_fasilitas,
                        "1"=>$row->keterangan,
                        "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_fasilitas_umum.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-info btn-sm" onclick="delete_data('.$row->id_fasilitas_umum.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                    );
                }
            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data
            );
            echo json_encode($result);
            break;

            case 'show' :
                $response = $fasilitas_umum->show($id_fasilitas_umum);
                echo json_encode($response);
            break;

            case 'delete_data' :
                $response = $fasilitas_umum->delete_data($id_fasilitas_umum);
            break;
            
    }